from datetime import date
from rest_framework.serializers import (
    ModelSerializer
)

from project.exceptions import CardExistsException, ExpiredCardException
from users.serializers import UserSerializer

from .models import PaymentInfo


class PaymentInfoSerializer(ModelSerializer):

    customer = UserSerializer(read_only=True)

    class Meta:
        model = PaymentInfo
        fields = '__all__'

        extra_kwargs = {
            'cvv': {'write_only': True}
        }

    def validate(self, attrs):

        expiring_date = attrs['card_expiring_date']
        payment_method = attrs['payment_method']
        card_number = attrs['card_number']

        if expiring_date < date.today():
            raise ExpiredCardException()

        payment_info = PaymentInfo.objects.filter(card_number=card_number, payment_method=payment_method).first()
        if payment_info:
            raise CardExistsException()

        return super().validate(attrs)

    def create(self, validated_data):
        validated_data['customer'] = self.context['request'].user
        return PaymentInfo.objects.create(**validated_data)
