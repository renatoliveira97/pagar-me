from datetime import date
from django.test import TestCase
from payment_info.models import PaymentInfo

from users.models import User


class PaymentInfoModelTest(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.email = 'test@mail.com'
        cls.password = '1234'
        cls.first_name = 'test'
        cls.last_name = 'example'
        cls.is_seller = False
        cls.is_admin = False

        cls.user = User.objects.create_user(
            email = cls.email,
            password = cls.password,
            first_name = cls.first_name,
            last_name = cls.last_name,
            is_seller = cls.is_seller,
            is_admin = cls.is_admin
        )

        cls.payment_method = 'debit'
        cls.card_number = '1234567812345678'
        cls.cardholders_name = 'test'
        cls.card_expiring_date = date.today()
        cls.cvv = 456

        cls.payment_info = PaymentInfo.objects.create(
            payment_method = cls.payment_method,
            card_number = cls.card_number,
            cardholders_name = cls.cardholders_name,
            card_expiring_date = cls.card_expiring_date,
            cvv = cls.cvv,
            customer = cls.user
        )

    def test_payment_info_fields(self):
        self.assertIsInstance(self.payment_info.card_number, str)
        self.assertEqual(self.payment_info.card_number, self.card_number)
        self.assertIsInstance(self.payment_info.is_active, bool)
        self.assertEqual(self.payment_info.is_active, True)
        self.assertIsInstance(self.payment_info.customer, User)
        self.assertEqual(self.payment_info.customer.email, self.email)
        