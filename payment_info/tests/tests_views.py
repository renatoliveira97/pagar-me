from datetime import date
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from payment_info.models import PaymentInfo

from users.models import User


class ProductViewTest(APITestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.seller_user = User.objects.create_user(
            email = 'test@mail.com',
            password = '1234',
            first_name = 'test',
            last_name = 'example',
            is_seller = True,
            is_admin = False
        )

        cls.common_user = User.objects.create_user(
            email = 'test_2@mail.com',
            password = '1234',
            first_name = 'test',
            last_name = 'example',
            is_seller = False,
            is_admin = False
        )

    def test_customer_create_new_payment_info_success(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        payment_info_data = {
            'payment_method': 'debit',
            'card_number': '1234567812345678',
            'cardholders_name': 'test',
            'card_expiring_date': date.today(),
            'cvv': 456
        }

        response = self.client.post('/api/payment_info/', payment_info_data)

        self.assertEqual(response.status_code, 201)
        self.assertNotIn('cvv', response.json())
        self.assertIn('customer', response.json())

    def test_customer_try_to_register_expired_card_returns_400(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        payment_info_data = {
            'payment_method': 'debit',
            'card_number': '1234567812345678',
            'cardholders_name': 'test',
            'card_expiring_date': '2022-01-01',
            'cvv': 456
        }

        response = self.client.post('/api/payment_info/', payment_info_data)
        data = response.json()

        self.assertEqual(response.status_code, 400)
        self.assertEqual(data['error'], ["This card is expired"])

    def test_customer_try_to_register_existent_card_returns_422(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        payment_info_data = {
            'payment_method': 'debit',
            'card_number': '1234567812345678',
            'cardholders_name': 'test',
            'card_expiring_date': date.today(),
            'cvv': 456
        }

        payment_info_data_2 = {
            'payment_method': 'credit',
            'card_number': '1234567812345678',
            'cardholders_name': 'test',
            'card_expiring_date': date.today(),
            'cvv': 456
        }

        self.client.post('/api/payment_info/', payment_info_data)
        response = self.client.post('/api/payment_info/', payment_info_data_2)

        self.assertEqual(response.status_code, 201)

        response = self.client.post('/api/payment_info/', payment_info_data)
        data = response.json()

        self.assertEqual(response.status_code, 422)
        self.assertEqual(data['error'], ["This card is already registered for this user"])

    def test_seller_create_new_payment_info_returns_403(self):

        token = Token.objects.create(user=self.seller_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        payment_info_data = {
            'payment_method': 'debit',
            'card_number': '1234567812345678',
            'cardholders_name': 'test',
            'card_expiring_date': date.today(),
            'cvv': 456
        }

        response = self.client.post('/api/payment_info/', payment_info_data)
        data = response.json()

        self.assertEqual(response.status_code, 403)
        self.assertEqual(data['detail'], 'You do not have permission to perform this action.')

    def test_no_token_create_new_payment_info_returns_401(self):

        payment_info_data = {
            'payment_method': 'debit',
            'card_number': '1234567812345678',
            'cardholders_name': 'test',
            'card_expiring_date': date.today(),
            'cvv': 456
        }

        response = self.client.post('/api/payment_info/', payment_info_data)
        data = response.json()

        self.assertEqual(response.status_code, 401)
        self.assertEqual(data['detail'], 'Authentication credentials were not provided.')

    def test_customer_create_new_payment_info_with_invalid_input(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        payment_info_data = {
            'payment_method': 'debit',
            'card_number': '1234567812345678',
            'card_expiring_date': date.today(),
            'cvv': 456
        }

        response = self.client.post('/api/payment_info/', payment_info_data)

        self.assertEqual(response.status_code, 400)

    def test_customer_list_your_payment_info_with_success(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        PaymentInfo.objects.create(
            payment_method = 'debit',
            card_number = '1234567812345678',
            cardholders_name = 'test',
            card_expiring_date = date.today(),
            cvv = 456,
            customer = self.seller_user
        )

        response = self.client.get('/api/payment_info/')
        data = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, [])

        own_payment_method = PaymentInfo.objects.create(
            payment_method = 'debit',
            card_number = '1234567812345679',
            cardholders_name = 'test2',
            card_expiring_date = date.today(),
            cvv = 458,
            customer = self.common_user
        )

        response = self.client.get('/api/payment_info/')
        data = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data[0]['card_number'], own_payment_method.card_number)
