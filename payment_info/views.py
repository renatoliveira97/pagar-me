from rest_framework.generics import (
    ListCreateAPIView
)
from rest_framework.authentication import TokenAuthentication
from project.permissions import OnlyCustomer
from payment_info.models import PaymentInfo
from payment_info.serializers import PaymentInfoSerializer
from rest_framework.response import Response
from rest_framework import status


class PaymentInfoListCreateView(ListCreateAPIView):

    queryset = PaymentInfo.objects.all()
    serializer_class = PaymentInfoSerializer

    authentication_classes = [TokenAuthentication]
    permission_classes = [OnlyCustomer]

    def get(self, request, *args, **kwargs):
        payment_infos = PaymentInfo.objects.filter(customer=request.user)
        serialized = PaymentInfoSerializer(payment_infos, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)