from django.urls import path
from .views import PaymentInfoListCreateView


urlpatterns = [
    path('payment_info/', PaymentInfoListCreateView.as_view())
]
