from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from fees.models import Fee

from users.models import User


class FeeViewTest(APITestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.admin_user = User.objects.create_user(
            email = 'test@mail.com',
            password = '1234',
            first_name = 'test',
            last_name = 'example',
            is_seller = False,
            is_admin = True
        )

        cls.common_user = User.objects.create_user(
            email = 'test_2@mail.com',
            password = '1234',
            first_name = 'test',
            last_name = 'example',
            is_seller = False,
            is_admin = False
        )

    def test_admin_create_new_fee_success(self):

        token = Token.objects.create(user=self.admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        fee_data = {
            'credit_fee': 0.05,
            'debit_fee': 0.03
        }

        response = self.client.post('/api/fee/', fee_data)

        self.assertEqual(response.status_code, 201)

    def test_common_user_create_new_fee_returns_403(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        fee_data = {
            'credit_fee': 0.05,
            'debit_fee': 0.03
        }

        response = self.client.post('/api/fee/', fee_data)
        data = response.json()

        self.assertEqual(response.status_code, 403)
        self.assertEqual(data['detail'], 'You do not have permission to perform this action.')

    def test_no_token_returns_401(self):

        fee_data = {
            'credit_fee': 0.05,
            'debit_fee': 0.03
        }

        response = self.client.post('/api/fee/', fee_data)
        data = response.json()

        self.assertEqual(response.status_code, 401)
        self.assertEqual(data['detail'], 'Authentication credentials were not provided.')

    def test_admin_create_new_fee_with_invalid_input(self):

        token = Token.objects.create(user=self.admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        fee_data = {
            'credit_fee': 0.05
        }

        response = self.client.post('/api/fee/', fee_data)

        self.assertEqual(response.status_code, 400)

    def test_admin_list_all_fees_with_success(self):

        token = Token.objects.create(user=self.admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        Fee.objects.create(
            credit_fee = 0.05,
            debit_fee = 0.03
        )

        response = self.client.get('/api/fee/')
        data = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data[0]['credit_fee'], 0.05)

    def test_common_user_list_all_fees_returns_403(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        Fee.objects.create(
            credit_fee = 0.05,
            debit_fee = 0.03
        )

        response = self.client.get('/api/fee/')
        data = response.json()

        self.assertEqual(response.status_code, 403)
        self.assertEqual(data['detail'], 'You do not have permission to perform this action.')

    def test_no_token_list_all_fees_returns_401(self):

        Fee.objects.create(
            credit_fee = 0.05,
            debit_fee = 0.03
        )

        response = self.client.get('/api/fee/')
        data = response.json()

        self.assertEqual(response.status_code, 401)
        self.assertEqual(data['detail'], 'Authentication credentials were not provided.')

    def test_admin_filter_fee_with_success(self):

        token = Token.objects.create(user=self.admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        fee_data = Fee.objects.create(
            credit_fee = 0.05,
            debit_fee = 0.03
        )

        fee_id = fee_data.id

        response = self.client.get(f'/api/fee/{fee_id}/')
        data = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['credit_fee'], 0.05)

    def test_common_user_filter_fee_returns_403(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        fee_data = Fee.objects.create(
            credit_fee = 0.05,
            debit_fee = 0.03
        )

        fee_id = fee_data.id

        response = self.client.get(f'/api/fee/{fee_id}/')
        data = response.json()

        self.assertEqual(response.status_code, 403)
        self.assertEqual(data['detail'], 'You do not have permission to perform this action.')

    def test_no_token_filter_fee_returns_401(self):

        fee_data = Fee.objects.create(
            credit_fee = 0.05,
            debit_fee = 0.03
        )

        fee_id = fee_data.id

        response = self.client.get(f'/api/fee/{fee_id}/')
        data = response.json()

        self.assertEqual(response.status_code, 401)
        self.assertEqual(data['detail'], 'Authentication credentials were not provided.')

    def test_admin_filter_fee_with_invalid_fee_id(self):

        token = Token.objects.create(user=self.admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        fee_id = '8888-8888-8888-8888'

        response = self.client.get(f'/api/fee/{fee_id}/')

        self.assertEqual(response.status_code, 404)
