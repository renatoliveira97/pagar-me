from rest_framework.generics import ListCreateAPIView, RetrieveAPIView
from rest_framework.authentication import TokenAuthentication
from fees.models import Fee
from fees.serializers import FeeSerializer
from project.permissions import OnlyAdmin


class FeeListCreateView(ListCreateAPIView):

    queryset = Fee.objects.all()
    serializer_class = FeeSerializer

    authentication_classes = [TokenAuthentication]
    permission_classes = [OnlyAdmin]

class FeeRetrieveView(RetrieveAPIView):

    queryset = Fee.objects.all()
    serializer_class = FeeSerializer
    lookup_url_kwarg = 'fee_id'

    authentication_classes = [TokenAuthentication]
    permission_classes = [OnlyAdmin]
