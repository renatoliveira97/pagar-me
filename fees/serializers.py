from rest_framework.serializers import (
    ModelSerializer
)

from .models import Fee


class FeeSerializer(ModelSerializer):

    class Meta:
        model = Fee
        fields = '__all__'

        extra_kwargs = {
            'created_at': {'read_only': True}
        }

    def create(self, validated_data):
        return super().create(validated_data)
