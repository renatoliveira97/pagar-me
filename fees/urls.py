from django.urls import path
from .views import FeeListCreateView, FeeRetrieveView


urlpatterns = [
    path('fee/', FeeListCreateView.as_view()),
    path('fee/<fee_id>/', FeeRetrieveView.as_view())
]
