from django.http import Http404
from rest_framework.permissions import BasePermission

from products.models import Product


class OnlyAdminListUsers(BasePermission):
    
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True

        return request.user and request.user.is_authenticated and request.user.is_admin == True

class OnlyAdmin(BasePermission):
    
    def has_permission(self, request, view):

        return request.user and request.user.is_authenticated and request.user.is_admin == True and request.user.is_seller == False

class OnlySellerCreateProduct(BasePermission):
    
    def has_permission(self, request, view):
        if request.method == 'GET':
            return request.user and request.user.is_authenticated

        return request.user and request.user.is_authenticated and request.user.is_admin == False and request.user.is_seller == True

class OnlyOwnerUpdateProduct(BasePermission):
    
    def has_permission(self, request, view):
        if request.method == 'GET':
            return True

        kwargs = view.__dict__['kwargs']

        product_id = kwargs['product_id']

        try:
            product = Product.objects.filter(id=product_id).first() 
        except: 
            raise Http404

        return request.user and request.user.is_authenticated and request.user.is_admin == False and request.user.is_seller == True and request.user.id == product.seller.id

class OnlyCustomer(BasePermission):
    
    def has_permission(self, request, view):

        return request.user and request.user.is_authenticated and request.user.is_admin == False and request.user.is_seller == False
