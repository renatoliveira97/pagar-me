from rest_framework.exceptions import APIException


class InvalidPriceException(APIException):
    status_code = 400
    default_detail = {'error': ['Minimum price is 0']}

class ExpiredCardException(APIException):
    status_code = 400
    default_detail = {'error': ["This card is expired"]}

class CardExistsException(APIException):
    status_code = 422
    default_detail = {'error': ["This card is already registered for this user"]}
