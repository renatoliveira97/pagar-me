from rest_framework.serializers import (
    ModelSerializer
)

from project.exceptions import InvalidPriceException
from users.serializers import UserSerializer

from .models import Product


class ProductSerializer(ModelSerializer):

    seller = UserSerializer(read_only=True)

    class Meta:
        model = Product
        fields = '__all__'

    def validate(self, attrs):

        price = attrs['price']

        if price < 0:
            raise InvalidPriceException()

        return super().validate(attrs)

    def create(self, validated_data):
        validated_data['seller'] = self.context['request'].user
        return Product.objects.create(**validated_data)
