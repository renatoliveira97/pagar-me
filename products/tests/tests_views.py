from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from products.models import Product

from users.models import User


class ProductViewTest(APITestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.seller_user = User.objects.create_user(
            email = 'test@mail.com',
            password = '1234',
            first_name = 'test',
            last_name = 'example',
            is_seller = True,
            is_admin = False
        )

        cls.common_user = User.objects.create_user(
            email = 'test_2@mail.com',
            password = '1234',
            first_name = 'test',
            last_name = 'example',
            is_seller = False,
            is_admin = False
        )

    def test_seller_create_new_product_success(self):

        token = Token.objects.create(user=self.seller_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        product_data = {
            'description': 'A simple description',
            'price': 10.0,
            'quantity': 5
        }

        response = self.client.post('/api/products/', product_data)

        self.assertEqual(response.status_code, 201)
        self.assertIn('seller', response.json())

    def test_seller_create_new_product_with_invalid_price(self):

        token = Token.objects.create(user=self.seller_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        product_data = {
            'description': 'A simple description',
            'price': -10.0,
            'quantity': 5
        }

        response = self.client.post('/api/products/', product_data)
        data = response.json()

        self.assertEqual(response.status_code, 400)
        self.assertEqual(data['error'], ['Minimum price is 0'])

    def test_common_user_create_new_product_returns_403(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        product_data = {
            'description': 'A simple description',
            'price': 10.0,
            'quantity': 5
        }

        response = self.client.post('/api/products/', product_data)
        data = response.json()

        self.assertEqual(response.status_code, 403)
        self.assertEqual(data['detail'], 'You do not have permission to perform this action.')

    def test_no_token_create_new_product_returns_401(self):

        product_data = {
            'description': 'A simple description',
            'price': 10.0,
            'quantity': 5
        }

        response = self.client.post('/api/products/', product_data)
        data = response.json()

        self.assertEqual(response.status_code, 401)
        self.assertEqual(data['detail'], 'Authentication credentials were not provided.')

    def test_seller_create_new_product_with_invalid_input(self):

        token = Token.objects.create(user=self.seller_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        product_data = {
            'description': 'A simple description',
            'price': 10.0
        }

        response = self.client.post('/api/products/', product_data)

        self.assertEqual(response.status_code, 400)

    def test_any_user_list_all_products_with_success(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        Product.objects.create(
            description = 'A simple description',
            price = 10.0,
            quantity = 5,
            seller = self.seller_user
        )

        response = self.client.get('/api/products/')
        data = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data[0]['price'], 10.0)

    def test_any_user_filter_product_with_success(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        product = Product.objects.create(
            description = 'A simple description',
            price = 10.0,
            quantity = 5,
            seller = self.seller_user
        )

        product_id = product.id

        response = self.client.get(f'/api/products/{product_id}/')
        data = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['price'], 10.0)

    def test_any_user_filter_product_with_invalid_product_id(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        product_id = '8888-8888-8888-8888'

        response = self.client.get(f'/api/products/{product_id}/')
        data = response.json()

        self.assertEqual(response.status_code, 404)

    def test_seller_update_her_own_product_with_success(self):

        token = Token.objects.create(user=self.seller_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        product = Product.objects.create(
            description = 'A simple description',
            price = 10.0,
            quantity = 5,
            seller = self.seller_user
        )

        product_id = product.id

        update_data = {
            'description': 'A new description',
            'price': 20.0,
            'quantity': 10,
            'is_active': False
        }

        response = self.client.patch(f'/api/products/{product_id}/', update_data)
        data = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['description'], update_data['description'])
        self.assertEqual(data['is_active'], False)

    def test_common_user_try_to_update_her_own_product_returns_403(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        product = Product.objects.create(
            description = 'A simple description',
            price = 10.0,
            quantity = 5,
            seller = self.common_user
        )

        product_id = product.id

        update_data = {
            'description': 'A new description',
            'price': 20.0,
            'quantity': 10,
            'is_active': False
        }

        response = self.client.patch(f'/api/products/{product_id}/', update_data)
        data = response.json()

        self.assertEqual(response.status_code, 403)
        self.assertEqual(data['detail'], 'You do not have permission to perform this action.')

    def test_no_token_try_to_update_product_returns_401(self):

        product = Product.objects.create(
            description = 'A simple description',
            price = 10.0,
            quantity = 5,
            seller = self.common_user
        )

        product_id = product.id

        update_data = {
            'description': 'A new description',
            'price': 20.0,
            'quantity': 10,
            'is_active': False
        }

        response = self.client.patch(f'/api/products/{product_id}/', update_data)
        data = response.json()

        self.assertEqual(response.status_code, 401)
        self.assertEqual(data['detail'], 'Authentication credentials were not provided.')

    def test_seller_update_another_person_product_returns_403(self):

        token = Token.objects.create(user=self.seller_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        product = Product.objects.create(
            description = 'A simple description',
            price = 10.0,
            quantity = 5,
            seller = self.common_user
        )

        product_id = product.id

        update_data = {
            'description': 'A new description',
            'price': 20.0,
            'quantity': 10,
            'is_active': False
        }

        response = self.client.patch(f'/api/products/{product_id}/', update_data)
        data = response.json()

        self.assertEqual(response.status_code, 403)
        self.assertEqual(data['detail'], 'You do not have permission to perform this action.')

    def test_seller_try_to_update_a_product_with_invalid_product_id(self):

        token = Token.objects.create(user=self.seller_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        product_id = '8888-8888-8888-8888'

        update_data = {
            'description': 'A new description',
            'price': 20.0,
            'quantity': 10,
            'is_active': False
        }

        response = self.client.patch(f'/api/products/{product_id}/', update_data)
        data = response.json()

        self.assertEqual(response.status_code, 404)

    def test_any_user_list_all_seller_products_with_success(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        Product.objects.create(
            description = 'A simple description',
            price = 10.0,
            quantity = 5,
            seller = self.seller_user
        )

        seller_id = self.seller_user.id

        response = self.client.get(f'/api/products/seller/{seller_id}/')
        data = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data[0]['price'], 10.0)

    def test_any_user_list_all_seller_products_with_invalid_seller_id(self):

        token = Token.objects.create(user=self.common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        seller_id = '8888-8888-8888-8888'

        response = self.client.get(f'/api/products/seller/{seller_id}/')
        data = response.json()

        self.assertEqual(response.status_code, 404)
