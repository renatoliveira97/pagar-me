from django.test import TestCase
from products.models import Product

from users.models import User


class ProductModelTest(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.email = 'test@mail.com'
        cls.password = '1234'
        cls.first_name = 'test'
        cls.last_name = 'example'
        cls.is_seller = True
        cls.is_admin = False

        cls.user = User.objects.create_user(
            email = cls.email,
            password = cls.password,
            first_name = cls.first_name,
            last_name = cls.last_name,
            is_seller = cls.is_seller,
            is_admin = cls.is_admin
        )

        cls.description = 'A simple description'
        cls.price = 10.0
        cls.quantity = 5

        cls.product = Product.objects.create(
            description = cls.description,
            price = cls.price,
            quantity = cls.quantity,
            seller = cls.user
        )

    def test_product_fields(self):
        self.assertIsInstance(self.product.description, str)
        self.assertEqual(self.product.description, self.description)
        self.assertIsInstance(self.product.price, float)
        self.assertEqual(self.product.price, self.price)
        self.assertIsInstance(self.product.is_active, bool)
        self.assertEqual(self.product.is_active, True)
        self.assertIsInstance(self.product.seller, User)
        self.assertEqual(self.product.seller.email, self.email)
        