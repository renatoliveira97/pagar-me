from django.urls import path
from .views import ProductListCreateView, ProductRetrieveUpdateView, SellerProductsRetrieveView


urlpatterns = [
    path('products/', ProductListCreateView.as_view()),
    path('products/<product_id>/', ProductRetrieveUpdateView.as_view()),
    path('products/seller/<seller_id>/', SellerProductsRetrieveView.as_view())
]
