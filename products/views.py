from django.http import Http404
from rest_framework.generics import (
    ListCreateAPIView, 
    RetrieveUpdateAPIView,
    RetrieveAPIView
)
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from rest_framework import status
from products.models import Product

from products.serializers import ProductSerializer
from project.permissions import OnlyOwnerUpdateProduct, OnlySellerCreateProduct
from users.models import User


class ProductListCreateView(ListCreateAPIView):

    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    authentication_classes = [TokenAuthentication]
    permission_classes = [OnlySellerCreateProduct]

class ProductRetrieveUpdateView(RetrieveUpdateAPIView):

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    lookup_url_kwarg = 'product_id'

    authentication_classes = [TokenAuthentication]
    permission_classes = [OnlyOwnerUpdateProduct]

class SellerProductsRetrieveView(RetrieveAPIView):
    
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    lookup_url_kwarg = 'seller_id'

    def get(self, request, *args, **kwargs):
        try:
            seller = User.objects.filter(id=kwargs['seller_id']).first()
        except:
            raise Http404
        products = Product.objects.filter(seller=seller)
        serialized = ProductSerializer(products, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)
