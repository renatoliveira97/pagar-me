from django.db import models
import uuid


class Product(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    description = models.TextField()

    price = models.FloatField()
    
    quantity = models.IntegerField()

    is_active = models.BooleanField(default=True)

    seller = models.ForeignKey('users.User', related_name='products', on_delete=models.CASCADE)