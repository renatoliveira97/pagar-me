from django.test import TestCase

from users.models import User


class UserModelTest(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.email = 'test@mail.com'
        cls.password = '1234'
        cls.first_name = 'test'
        cls.last_name = 'example'
        cls.is_seller = True
        cls.is_admin = True

        cls.user = User.objects.create_user(
            email = cls.email,
            password = cls.password,
            first_name = cls.first_name,
            last_name = cls.last_name,
            is_seller = cls.is_seller,
            is_admin = cls.is_admin
        )

    def test_user_fields(self):
        self.assertIsInstance(self.user.email, str)
        self.assertEqual(self.user.email, self.email)
        self.assertIsInstance(self.user.is_seller, bool)
        self.assertEqual(self.user.is_seller, self.is_seller)
