from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from users.models import User


class UserViewTest(APITestCase):

    def test_create_new_user_success(self):
        user_data = {
            'email': 'test@mail.com',
            'password': '1234',
            'first_name': 'test',
            'last_name': 'example',
            'is_seller': False,
            'is_admin': False
        }

        response = self.client.post('/api/accounts/', user_data)

        self.assertEqual(response.status_code, 201)

        self.assertNotIn('password', response.json())

    def test_create_new_seller_success(self):
        seller_data = {
            'email': 'test@mail.com',
            'password': '1234',
            'first_name': 'test',
            'last_name': 'example',
            'is_seller': True,
            'is_admin': False
        }

        response = self.client.post('/api/accounts/', seller_data)
        data = response.json()

        self.assertEqual(response.status_code, 201)

        self.assertEqual(data['is_seller'], True)

    def test_create_new_admin_success(self):
        admin_data = {
            'email': 'test@mail.com',
            'password': '1234',
            'first_name': 'test',
            'last_name': 'example',
            'is_seller': False,
            'is_admin': True
        }

        response = self.client.post('/api/accounts/', admin_data)
        data = response.json()

        self.assertEqual(response.status_code, 201)

        self.assertEqual(data['is_admin'], True)

    def test_return_400_when_try_to_create_an_existent_user(self):
        user_data = {
            'email': 'test@mail.com',
            'password': '1234',
            'first_name': 'test',
            'last_name': 'example',
            'is_seller': False,
            'is_admin': False
        }

        self.client.post('/api/accounts/', user_data)
        response = self.client.post('/api/accounts/', user_data)
        data = response.json()

        self.assertEqual(response.status_code, 400)

        self.assertEqual(data['email'], ['user with this email already exists.'])

    def test_return_400_when_try_to_create_an_user_with_invalid_data(self):
        user_data = {
            'email': 'test@mail.com',
            'first_name': 'test',
            'last_name': 'example',
            'is_seller': False,
            'is_admin': False
        }

        self.client.post('/api/accounts/', user_data)
        response = self.client.post('/api/accounts/', user_data)

        self.assertEqual(response.status_code, 400)

    def test_admin_list_users_success(self):
        admin_user = User.objects.create_user(
            email = 'test@mail.com',
            password = '1234',
            first_name = 'test',
            last_name = 'example',
            is_seller = False,
            is_admin = True
        )

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        response = self.client.get('/api/accounts/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()[0]['first_name'], 'test')

    def test_common_user_list_users_returns_403(self):
        user = User.objects.create_user(
            email = 'test_2@mail.com',
            password = '1234',
            first_name = 'test',
            last_name = 'example',
            is_seller = False,
            is_admin = False
        )

        token = Token.objects.create(user=user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        response = self.client.get('/api/accounts/')
        data = response.json()

        self.assertEqual(response.status_code, 403)
        self.assertEqual(data['detail'], 'You do not have permission to perform this action.')

    def test_no_token_returns_401(self):

        response = self.client.get('/api/accounts/')
        data = response.json()

        self.assertEqual(response.status_code, 401)
        self.assertEqual(data['detail'], 'Authentication credentials were not provided.')

class LoginViewTest(APITestCase):
    def setUp(self) -> None:
        User.objects.create_user(
            email = 'test@mail.com',
            password = '1234',
            first_name = 'test',
            last_name = 'example',
            is_seller = False,
            is_admin = False
        )

    def test_login_success(self):
        login_data = {
            'email': 'test@mail.com',
            'password': '1234'
        }

        response = self.client.post('/api/login/', login_data)

        self.assertEqual(response.status_code, 200)
        self.assertIn('token', response.json())

    def test_return_400_when_try_to_login_with_invalid_data(self):
        login_data = {
            'email': 'test@mail.com'
        }

        response = self.client.post('/api/login/', login_data)

        self.assertEqual(response.status_code, 400)

    def test_return_401_when_try_to_login_with_invalid_credentials(self):
        login_data = {
            'email': 'test@mail.com',
            'password': 'abcd'
        }

        response = self.client.post('/api/login/', login_data)

        self.assertEqual(response.status_code, 401)
