from rest_framework.generics import GenericAPIView
from rest_framework.views import APIView, Response, status
from rest_framework.mixins import CreateModelMixin, ListModelMixin
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from project.permissions import OnlyAdminListUsers

from users.models import User
from users.serializers import UserLoginSerializer, UserSerializer


class UserListCreateView(GenericAPIView, CreateModelMixin, ListModelMixin):

    queryset = User.objects.all()
    serializer_class = UserSerializer

    authentication_classes = [TokenAuthentication]
    permission_classes = [OnlyAdminListUsers]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class LoginView(APIView):

    def post(self, request):

        serializer = UserLoginSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(**serializer.validated_data)

        if user:
            token = Token.objects.get_or_create(user=user)[0]
            return Response({'token': token.key})

        return Response({'error': 'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)
