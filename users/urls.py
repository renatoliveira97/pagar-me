from django.urls import path
from .views import LoginView, UserListCreateView


urlpatterns = [
    path('accounts/', UserListCreateView.as_view()),
    path('login/', LoginView.as_view())
]
