from rest_framework.serializers import (
    ModelSerializer, 
    Serializer,
    EmailField,
    CharField
)

from .models import User


class UserSerializer(ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'email', 'password', 'first_name', 'last_name', 'is_seller', 'is_admin']

        extra_kwargs = {
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        return super().create(validated_data)

class UserLoginSerializer(Serializer):
    email = EmailField()
    password = CharField(write_only=True)
